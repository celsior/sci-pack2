#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <map>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <functional>

#include <signal.h>
#include <time.h>

#include "driver/driver.h"

using std::istream;
using std::ostream;
using std::min;
using std::max;

struct bbox
{
  int x_min;
  int x_max;
  int y_min;
  int y_max;
  int z_min;
  int z_max;
};

double rnd(double mag = 1, double min = 0)
{
  return double(rand())/RAND_MAX * mag + min;
}

double sqr(double v)
{
  return v*v;
}

double cbk(double v)
{
  return v*v*v;
}

double gauss(double mu = 0, double sigma = 1)
{
  return mu + sigma * sqrt(-2 * log(rnd())) * cos(2*M_PI*rnd());
}



struct vector
{
public:
  double x;
  double y;
  double z;

  vector(): x(0), y(0), z(0) {};
  vector(double x, double y, double z): x(x), y(y), z(z) {};
  vector(const vector& v): x(v.x), y(v.y), z(v.z) {};
  vector(istream& str);

  double length() const;
  double length2() const;
  vector orth() const;

  void operator+=(const vector& v);
};

vector operator*(const vector& v, const double s)
{
  return vector(v.x*s, v.y*s, v.z*s);
};

vector operator*(const double s, const vector& v)
{
  return vector(v.x*s, v.y*s, v.z*s);
};

vector operator/(const vector& v, const double s)
{
  return vector(v.x/s, v.y/s, v.z/s);
};

vector::vector(istream& str)
{
  str >> x >> y >> z;
}

double vector::length() const
{
  return sqrt(x*x + y*y + z*z);
}

double vector::length2() const
{
  return x*x + y*y + z*z;
}


vector vector::orth() const
{
  const double len = length();
  if (len != 0)
    return (*this)/len;
  else
    return vector(0, 0, 0);
}

ostream& operator<<(ostream& str, const vector& v)
{
  str << v.x << " " << v.y << " " << v.z;
  return str;
}

istream& operator>>(istream& str, vector& v)
{
  str >> v.x >> v.y >> v.z;
  return str;
}

void vector::operator+=(const vector& v)
{
  x += v.x;
  y += v.y;
  z += v.z;
}

bool operator==(const vector& v1, const vector& v2)
{
  return (v1.x == v2.x) && (v1.y == v2.y) && (v1.z == v2.z);
}

bool operator!=(const vector& v1, const vector& v2)
{
  return (v1.x != v2.x) || (v1.y != v2.y) || (v1.z != v2.z);
}


vector operator+(const vector& v1, const vector& v2)
{
  return vector(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z);
}

vector operator-(const vector& v1, const vector& v2)
{
  return vector(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}


vector min_point;
vector max_point;

int iterations = 100;
double tolerance;

double E;
double T;
double grow_rate;

double sz = 0.1;
double max_sz = sz;
double Vtotal = 0;
double V0 = 0;
double Vparticles = 0;

int overlapped = 0;

class flipper
{
public:
  const vector& center;
  const double radius;

  const vector& lo;
  const vector& hi;

  unsigned int n;
  unsigned int pat;
  vector tmp;

  enum {NF = 1, FX = 2, FY = 4, FZ = 8, FXY = 16, FXZ = 32, FXYZ = 64, FYZ = 128};

  flipper(const vector& center, const double radius, const vector& min_bound, const vector& max_bound)
    : center(center), radius(radius), lo(min_bound), hi(max_bound), n(1)
  {
    unsigned int fx = (center.x - radius < lo.x || center.x + radius > hi.x);
    unsigned int fy = (center.y - radius < lo.y || center.y + radius > hi.y);
    unsigned int fz = (center.z - radius < lo.z || center.z + radius > hi.z);

    pat = 1 + (fx<<1) + (fy<<2) + (fz<<3) + ((fx&&fy)<<4) + ((fx&&fz)<<5) + ((fx&&fy&&fx)<<6) + ((fy&&fz)<<7);
  };

  bool flip(double& v, double lo, double hi) const
  {
    if (v - radius < lo)
      {
        v = hi + (v - lo);
        return true;
      }
    else if (v + radius > hi)
      {
        v = lo - (hi - v);
        return true;
      }

    return false;
  }

  vector const* flip()
  {
    tmp = center;

    while (pat)
      {
        bool f = pat & n;
        if (f)
          {
            pat ^= n;
            switch (n)
              {
              case NF:
                break;
              case FX:
                flip(tmp.x, lo.x, hi.x); break;
              case FY:
                flip(tmp.y, lo.y, hi.y); break;
              case FZ:
                flip(tmp.z, lo.z, hi.z); break;
              case FXY:
                flip(tmp.x, lo.x, hi.x); flip(tmp.y, lo.z, hi.y); break;
              case FXZ:
                flip(tmp.x, lo.x, hi.x); flip(tmp.z, lo.z, hi.z); break;
              case FXYZ:
                flip(tmp.x, lo.x, hi.x); flip(tmp.y, lo.y, hi.y); flip(tmp.z, lo.z, hi.z); break;
              case FYZ:
                flip(tmp.y, lo.y, hi.y); flip(tmp.z, lo.z, hi.z); break;
              default:
                throw std::runtime_error("unreacheable");
              }
          }
        n <<= 1;
        if (f)
          return &tmp;
      }

    return 0;
  }
};

double lens_volume(double r1, double r2, double d)
{
  return  max(0.0, M_PI * sqr(r1+r2-d) * (sqr(d) + 2*d*r1 - 3*sqr(r1) + 2*d*r2 + 6*r1*r2 - 3*sqr(r2)) / 12.0 / d);
}


class particle
{
public:
  vector center;
  double curR;
  double baseR;
  std::string tag;
  std::string distr;
  bool fixed;
  double V;

  int N;
  int Nlc;
  double E;

  particle(const vector& center, double baseR, const std::string& tag, const std::string& distr, bool fixed)
    : center(center), curR(baseR), baseR(baseR), tag(tag), distr(distr), fixed(fixed), V(4.0/3 * M_PI * cbk(baseR)),
      N(0), Nlc(-1), E(0) {};

  particle(istream& str, int N, double grow_rate)
    : N(N), Nlc(-1), E(0)
  {
    str >> tag >> distr >> center >> baseR >> fixed;
    V = 4.0/3 * M_PI * cbk(baseR);
    curR = baseR;
  }

  vector min_bb_vector() const
  {
    return vector(center.x-curR, center.y-curR, center.z-curR);
  }

  vector max_bb_vector() const
  {
    return vector(center.x+curR, center.y+curR, center.z+curR);
  }

  double distance(const particle& other, vector* ncenter, vector* nother_center) const
  {
    double ret = 1e9;
    flipper f1(center, curR, min_point, max_point);

    while (const vector* p1 = f1.flip())
      {
        flipper f2(other.center, other.curR, min_point, max_point);
        while (const vector* p2 = f2.flip())
          {
            const double len = ((*p1)-(*p2)).length2();
            if (len < ret)
              {
                ret = len;
                if (ncenter) *ncenter = *p1;
                if (nother_center) *nother_center = *p2;
              }
          }
      }
    if (ret < 0)
      return 0;

    return sqrt(ret);
  }

  bool reaction(const particle& other, vector* react) const
  {
    vector ncenter, nothercenter;
    const double dist = distance(other, &ncenter, &nothercenter);
    const double d = max(curR, other.curR) - min(curR, other.curR);

    if (react)
      {
        if (dist > curR + other.curR || dist == 0)
          *react = vector(0,0,0);
        else if (dist < d)
          *react = (ncenter - nothercenter).orth()  * (lens_volume(curR, other.curR, d) / this->V);
        else
          *react = (ncenter - nothercenter).orth()  * (lens_volume(curR, other.curR, dist) / this->V);
      }

    return (dist != 0);
  }

  double reaction(const particle& other) const
  {
    const double dist = distance(other, nullptr, nullptr);
    const double d = max(curR, other.curR) - min(curR, other.curR);

    const double minR = min(curR, other.curR);

    if (dist == 0)
      return 1e6;

    if (dist > curR + other.curR)
      return 0;

    // if (dist < d)
    //   return 4.0/3*M_PI*cbk(minR) * exp((d - dist)/d) / this->V;   //lens_volume(curR, other.curR, d) / this->V;
    // else
    //   return lens_volume(curR, other.curR, dist) / this->V;

    return (curR+other.curR - dist) / this->V;
  }
};


ostream& operator<< (ostream& str, const particle& p)
{
  str << p.tag << " " << p.distr << " "
      << p.center << " " << p.baseR << " " << p.fixed;

  return str;
}



#include <boost/geometry.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/index/rtree.hpp>
#include <boost/geometry/geometries/register/point.hpp>
BOOST_GEOMETRY_REGISTER_POINT_3D(vector, double, cs::cartesian, x, y, z);
typedef boost::geometry::model::box<vector> box;
namespace bgi = boost::geometry::index;
typedef std::pair<box, particle*> value;
typedef std::unordered_set<particle*> particle_set;

class rtree_index
{
public:
  typedef bgi::quadratic<16> bgi_alg;
  bgi::rtree< value, bgi_alg > rtree;

  void init()
  {
  }

  void clear()
  {
    rtree.clear();
  }

  void add(particle* p)
  {
    flipper f1(p->center, p->curR, min_point, max_point);
    while (const vector* p1 = f1.flip())
      {
        box b = box(vector(p1->x - p->curR, p1->y - p->curR, p1->z - p->curR),
                    vector(p1->x + p->curR, p1->y + p->curR, p1->z + p->curR));
        rtree.insert(std::make_pair(b, p));
      }
  }

  void rem(particle* p)
  {
    flipper f1(p->center, p->curR, min_point, max_point);
    while (const vector* p1 = f1.flip())
      {
        box b = box(vector(p1->x - p->curR, p1->y - p->curR, p1->z - p->curR),
                    vector(p1->x + p->curR, p1->y + p->curR, p1->z + p->curR));
        rtree.remove(std::make_pair(b, p));
      }
  }

  void for_intersected(const particle* const p, void (*fn)(const particle* const base, particle* const p, void* data), void* data)
  {
    flipper f1(p->center, p->curR, min_point, max_point);
    while (const vector* p1 = f1.flip())
      {
        box b2 = box(vector(p1->x - p->curR, p1->y - p->curR, p1->z - p->curR),
                     vector(p1->x + p->curR, p1->y + p->curR, p1->z + p->curR));
        for (auto& v : rtree | bgi::adaptors::queried(bgi::intersects(b2)))
          fn(p, v.second, data);
      }
  }

};


typedef std::vector<particle*> particle_list;


rtree_index sp_index;
particle_list all_particles;
particle_list particles;
int Nparticles = 0;

bool outbound(vector* const p, double r)
{
  return
    (p->x < min_point.x-r) || (p->x > max_point.x+r) ||
    (p->y < min_point.y-r) || (p->y > max_point.y+r) ||
    (p->z < min_point.z-r) || (p->z > max_point.z+r);

  return
    (p->x < min_point.x+r) || (p->x > max_point.x-r) ||
    (p->y < min_point.y+r) || (p->y > max_point.y-r) ||
    (p->z < min_point.z+r) || (p->z > max_point.z-r);
}

void correct_vector(vector& p, double r)
{
  /*
  p->x = std::min(std::max(p->x, min_point.x+r), max_point.x-r);
  p->y = std::min(std::max(p->y, min_point.y+r), max_point.y-r);
  p->z = std::min(std::max(p->z, min_point.z+r), max_point.z-r);
  */


  if (p.x < min_point.x)
    p.x = max_point.x + (p.x - min_point.x);
  else if (p.x > max_point.x)
    p.x = min_point.x + (p.x - max_point.x);

  //p.x = std::min(std::max(p.x, min_point.x+r), max_point.x-r);

  if (p.y < min_point.y)
    p.y = max_point.y + (p.y - min_point.y);
  else if (p.y > max_point.y)
    p.y = min_point.y + (p.y - max_point.y);

  if (p.z < min_point.z)
    p.z = max_point.z + (p.z - min_point.z);
  else if (p.z > max_point.z)
    p.z = min_point.z + (p.z - max_point.z);
}


void add_react_f(const particle* const base, particle* const p, void* data)
{
  if (p->Nlc == base->N || base == p)
    return;

  double* magnitude = static_cast<double*>(data);
  *magnitude += base->reaction(*p);

  p->Nlc = base->N;
}

void drop_flag_f(const particle* const base, particle* const p, void* data)
{
  p->Nlc = -1;
}

void intersected_info_f(const particle* const base, particle* const p, void* data)
{
  if (base == p)
    return;

  double d = base->distance(*p,nullptr,nullptr) - p->curR - base->curR;
  std::cerr << base->N << " " << p->N << " " << d  << " " << p->E << " " << p->reaction(*base) << "\n";
}


void calc_reaction(particle* const p, double& magnitude)
{
  if (outbound(&p->center, p->curR))
    {
      magnitude += 1e15;
      return;
    }

  sp_index.for_intersected(p, add_react_f, (void*)&magnitude);
  sp_index.for_intersected(p, drop_flag_f, nullptr);
}

void react_f(const particle* const base, particle* const p, void* data)
{
  if (p == base)
    return;

  p->E += p->reaction(*base);
  if (p->E > tolerance)
    ++overlapped;
}


double calc_E()
{
  overlapped = 0;
  double E = 0;
  for (auto p : particles)
    {
      double r = 0;
      calc_reaction(p, r);
      p->E = r;
      E += r;
      if (r > tolerance)
        ++overlapped;
    }

  return E;
}

bool compare_particle_size(particle* p1, particle* p2)
{
  return p1->baseR < p2->baseR;
}


void read_particles(const command_t& args)
{
  std::ifstream str(args[1].c_str());
  if (! str.is_open())
    throw std::runtime_error("Cannot open file");

  char rectype = 0;
  str >> rectype;
  while (!str.eof() && (rectype != 'E') )
    {
      if (rectype == 'E')
        break;
      else if (rectype != 'P')
        throw std::runtime_error("Malformed file");;

      particle* p = new particle(str, ++Nparticles, grow_rate);
      all_particles.push_back(p);
      Vtotal += 4.0/3*M_PI*pow(p->baseR, 3);

      str >> rectype;
    }
  std::sort(all_particles.begin(), all_particles.end(), compare_particle_size);

  V0 = ((max_point.x-min_point.x) * (max_point.y-min_point.y) * (max_point.z-min_point.z));
  std::cerr << "READ " << Nparticles << " " << Vtotal / V0 << "\n";
}

double try_move(particle* const p, const vector& new_center)
{
  sp_index.rem(p);
  const vector old_center = p->center;

  double r0 = 0;
  double r1 = 0;
  calc_reaction(p, r0);
  p->center = new_center;
  calc_reaction(p, r1);
  if (r1 <= r0)
    {
      sp_index.add(p);
      return r1;
    }
  else
    {
      p->center = old_center;
      sp_index.add(p);
      return r0;
    }
}

void move(particle* const p, vector&& new_center)
{
  correct_vector(new_center, p->curR);
  sp_index.rem(p);
  p->center = new_center;
  double r1;
  calc_reaction(p, r1);
  p->E = r1;
  sp_index.add(p);
}



bool flip(double* v, double r, double D, double lo, double hi)
{
  if (*v - r < lo + D)
    {
      *v = hi + (*v - lo);
      return true;
    }
  else if (*v + r > hi - D)
    {
      *v = lo - (hi - *v);
      return true;
    }
  else
    return false;
}

void write_particle_list(particle_list& lst, ostream& str)
{
  for (particle_list::iterator it = lst.begin(); it != lst.end(); ++it)
    str << "P " << *(*it) << "\n";
}


void write_particles(const command_t& args)
{
  std::ofstream str(args[1].c_str());

  std::sort(particles.begin(), particles.end(), compare_particle_size);
  particle_list excluded;

  for (particle_list::iterator it = particles.begin(); it != particles.end(); )
    {
      particle* p = *it;
      double r_mag = 0;
      calc_reaction(p, r_mag);
      if (r_mag > tolerance)
        {
          if ((*it)->fixed)
            {
              std::cerr << "R " << *(*it) << "\n";
              continue;
            }

          sp_index.rem(p);
          excluded.push_back(p);
          it = particles.erase(it);
        }
      else
        ++it;
    }

  str.precision(25);
  write_particle_list(particles, str);
  str << "E\n";
  write_particle_list(excluded, str);
  str.precision(5);
}



double fluct_particle(particle* const p)
{
  double mag = T;
  double r = 0;
  for (int i = 0; i < 15; ++i)
    {
      const vector dir = vector(2*rnd()-1, 2*rnd()-1, 2*rnd()-1).orth();
      vector new_center = p->center + dir * mag*rnd() / p->curR;
      correct_vector(new_center, p->curR);
      r = try_move(p, new_center);
      if (r == 0)
        break;
      else
        mag /= 5;
    }
  return r;
}


void pack_particles_RS()
{
  for (int i = 0; i < iterations; ++i)
    {
      if (overlapped == 0)
        break;

      overlapped = 0;
      for (auto p : particles)
        {
          if (p->E == 0 || p->fixed)
            continue;

          p->E = fluct_particle(p);
          if (p->E > tolerance)
            {
              ++overlapped;
              sp_index.for_intersected(p, react_f, nullptr);
            }
        }
    }
}

particle* nearest;
double nearest_dist;

void find_nearest_f(const particle* const base, particle* const p, void* data)
{
  if (p->Nlc == base->N || base == p)
    return;

  double d = base->distance(*p, nullptr, nullptr) - base->curR - p->curR;
  if (d < nearest_dist)
    {
      nearest_dist = d;
      nearest = p;
    }
}

void displace_particle(particle* const p)
{
  nearest = nullptr;
  nearest_dist = 1e15;
  sp_index.for_intersected(p, find_nearest_f, nullptr);

  if (nearest_dist >= 0)
    return;

  vector nc;
  vector pc;
  nearest->distance(*p, &nc, &pc);

  const vector d = T * nearest_dist * (pc - nc).orth();
  double a = cbk(nearest->curR/p->curR);

  move(p, p->center - d / (1+a) * a);
  move(nearest, nearest->center + d / (1+a));

  if (p->E > tolerance)
    {
      ++overlapped;
      sp_index.for_intersected(p, react_f, nullptr);
    }

  if (nearest->E > tolerance)
    {
      ++overlapped;
      sp_index.for_intersected(nearest, react_f, nullptr);
    }
}


void pack_particles_JT()
{
  for (int i = 0; i < iterations; ++i)
    {
      if (overlapped == 0)
        break;

      overlapped = 0;
      for (auto p : particles)
        {
          if (p->E == 0 || p->fixed)
            continue;

          displace_particle(p);
        }
    }
}

typedef void(*pack_f)();
pack_f pack_particles = pack_particles_RS;


void place_all_particles(const command_t& args)
{
  while (! all_particles.empty())
    {
      auto p = all_particles.back();
      all_particles.pop_back();
      p->curR = p->baseR*sz;
      particles.push_back(p);
      sp_index.add(p);
      Vparticles += p->V;
    }
  E = calc_E();
}

void add_n_pack()
{
  if (overlapped == 0 && all_particles.empty())
    return;

  while (overlapped == 0 && !all_particles.empty())
    {
      auto p = all_particles.back();
      all_particles.pop_back();
      particles.push_back(p);
      sp_index.add(p);
      Vparticles += p->V;

      double r0 = 0;
      calc_reaction(p, r0);
      if (r0 > 0)
        {
          p->E = r0;
          ++overlapped;
          break;
        }
    }

  pack_particles();
  if (overlapped > 0)
    {
      for (auto it = particles.rbegin(); it != particles.rend(); ++it)
        {
          auto p = *it;
          if (p->E == 0)
            continue;
          try_move(p, vector(rnd()*max_point.x, rnd()*max_point.y, rnd()*max_point.z));
          sp_index.for_intersected(p, react_f, nullptr);
          break;
        }
    }
}

void shuffle(int cycles)
{
  sigint = false;
  signal(SIGINT, handle_sigint);

  for (int i = 0; i < cycles; ++i)
    {
      for (auto p : particles)
        fluct_particle(p);

      std::cerr << i << "\n";
      if (sigint)
        break;
    }
}


double shrink_rate = 0.05;
double old_sz = 0;
void grow_n_pack()
{
  if (old_sz != 1)
    {
      if (overlapped == 0)
        {
          sz = std::min(1.0, sz + grow_rate);

          //grow_rate *= 1.001;
          if (sz > max_sz)
            max_sz = sz;
        }
      else
        {
          sz = std::max(0.1, sz - shrink_rate);
          //grow_rate /= 1.006;
        }

      if (sz != old_sz)
        {
          sp_index.clear();
          for (auto it = particles.begin(); it != particles.end(); ++it)
            {
              (*it)->curR = (*it)->baseR*sz;
              sp_index.add(*it);
            }
          if (sz < old_sz)
            shuffle(1);
          E = calc_E();
        }
    }
  old_sz = sz;
  pack_particles();
}

struct PointParam : public Param
{
  vector* pval;
  PointParam(vector* pval)
  : pval(pval) {};

  void parse_and_set(const std::string& val);
  std::string get();
};

void PointParam::parse_and_set(const std::string& val)
{
  command_t tokens;
  tokenize(val, tokens, " ", true, 3);
  *pval = vector(str2double(tokens[0]), str2double(tokens[1]), str2double(tokens[2]));
}

std::string PointParam::get()
{
  std::stringstream out;
  out << (*pval);
  return out.str();
}



void shuffle_cmd(const command_t& args)
{
  int cycles = str2int(args[1]);
  shuffle(cycles);
}

int n;
void run_add(const command_t& args)
{
  n = 0;

  sigint = false;
  signal(SIGINT, handle_sigint);
  while (!sigint)
    {
      ++n;
      add_n_pack();
      if (all_particles.empty() && overlapped == 0)
        break;

      std::cerr << n << " " << particles.size() << "/" << Nparticles << " " << Vparticles/Vtotal << " " << Vparticles/V0 << " " << overlapped << " E = " << E << "\n";
    }
  std::cerr << n << " " << particles.size() << "/" << Nparticles << " " << Vparticles/Vtotal << " " << Vparticles/V0 << " " << overlapped << " E = " << E << "\n";
}

void run_grow(const command_t& args)
{
  n = 0;

  sigint = false;
  signal(SIGINT, handle_sigint);
  while (!sigint)
    {
      ++n;
      grow_n_pack();
      if (sz == 1 && overlapped == 0)
        break;

      std::cerr << n << " " << overlapped << " " << E << " | " << max_sz << " " << sz << " " << pow(sz,3)*Vtotal / V0 <<  " | " << T << "\n";
    }
  std::cerr << n << " " << overlapped << " " << E << " | " << max_sz << " " << sz << " " << pow(sz,3)*Vtotal / V0 <<  " | " << T << "\n";
}


void init(const command_t& args)
{
  sp_index.init();
}

void set_alg(const command_t& args)
{
  if (args[1] == "RS")
    pack_particles = pack_particles_RS;
  else if (args[1] == "JT")
    pack_particles = pack_particles_JT;
  else
    throw std::runtime_error("Unknown packing algorithm; specify 'RS' or 'JT'");
}


int main(int argc, char** argv)
{
  srand(time(0));

  init_commands({
    {".init", init},
    {".run_add", run_add},
    {".run_grow", run_grow},
    {".read_particles", read_particles},
    {".write_particles", write_particles},
    {".place_all_particles", place_all_particles},
    {".shuffle", shuffle_cmd},
    {".set_alg", set_alg},
  });

  init_params({
    {"tolerance", new DoubleParam(&tolerance)},
    {"grow_rate", new DoubleParam(&grow_rate)},
    {"shrink_rate", new DoubleParam(&shrink_rate)},

    {"min_point", new PointParam(&min_point)},
    {"max_point", new PointParam(&max_point)},

    {"iterations", new IntParam(&iterations)},
    {"T", new DoubleParam(&T)},
    {"sz", new DoubleParam(&sz)},
  });

  if (argc > 1)
    {
      if (! file_loop(argv[1]))
        {
          return 0;
        }
    }

  command_loop();

  return 0;
}
