#!/usr/bin/python

from sys import stdin, stdout, stderr
from sys import argv
from math import floor, pi, sqrt

from distributions import *

from numpy import random

import numpy as np

class record:
    pass

def random_point(max_point):
    return (
        random.uniform(0, max_point[0]),
        random.uniform(0, max_point[1]),
        random.uniform(0, max_point[2])
        )

def sqdist(p1, p2):
    return (p1[0]-p2[0])**2 + (p1[1]-p2[1])**2 + (p1[2]-p2[2])**2

class particle:
    def __init__(self, tag, distr, r, fixed, center=(0,0,0)):
        self.tag = tag
        self.distr = distr
        self.r = r
        self.fixed = fixed
        self.center = center


geom = record()
distrs = {}
tags = {}

particles = []


def max_point(x, y, z):
    geom.max_point = (x, y, z)
    geom.vol = x*y*z


def mode(tag, distr, vol_fraction, get_d, cutoff=5, excess=1.01):
    V = 0.0
    n = 0
    if not distr in distrs: distrs[distr] = 0
    if not tag in tags: tags[tag] = 0
    while V < geom.vol*vol_fraction:
        r = float(get_d())/2
        while r < 0:
            r = float(get_d())/2
        if V + 4.0/3 * pi * r**3 > geom.vol*vol_fraction*excess:
            print >>stderr, "too large r", r
            continue

        V += 4.0/3 * pi * r**3

        if r*2 > cutoff:
            p = particle(tag, distr, r, False)
            p.center = random_point(geom.max_point)
            particles.append(p)
        n += 1
    distrs[distr] += V / geom.vol
    tags[tag] += V / geom.vol
    print >>stderr, tag, distr, n, V / geom.vol


def fixed(tag, distr, x, y, z, r, ratio = 1):
    if not distr in distrs: distrs[distr] = 0
    if not tag in tags: tags[tag] = 0
    particles.append(particle(tag, distr, r, True, (x,y,z)))
    V = 4.0/3 * pi * r**3 * ratio
    distrs[distr] += V / geom.vol
    tags[tag] += V / geom.vol


def load(filename, load_all = False, fix = False):
    jammed = False
    for line in open(filename):
        ln = line.split()
        if ln[0] == 'E':
            if not load_all:
                break
            else:
                jammed = True

        if ln[0] != 'P':
            continue

        p = particle(ln[1], ln[2], float(ln[6]), fix or (int(ln[7]) == 1), (float(ln[3]), float(ln[4]), float(ln[5])))
        p.jammed = jammed
        particles.append(p)


def write():
    for p in particles:
        stdout.write("P %s %s %.25e %.25e %.25e %e %d\n" % (p.tag, p.distr, p.center[0], p.center[1], p.center[2], p.r, 1 if p.fixed else 0))

    stdout.write("E\n")
    for n,v in distrs.items():
        stdout.write("C (distr) %s %e\n" % (n, v))
    for n,v in tags.items():
        stdout.write("C (tag) %s %e\n" % (n, v))


if __name__ == '__main__':
    execfile(argv[1])
    write()
