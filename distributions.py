from numpy import random

def equal(r):
    return lambda: r

def uniform(low, high):
    return lambda: random.uniform(low, high)

def beta(a, b):
    return lambda: random.beta(a, b)

def binomial(n, p):
    return lambda: random.binomial(n, p)

def chisquare(df):
    return lambda: random.chisquare(df)

def dirichlet(alpha):
    return lambda: random.mtrand.dirichlet(alpha)

def exponential(scale = 1.0):
    return lambda: random.exponential(scale)

def f(dfnum, dfden):
    return lambda: random.f(dfnum, dfden)

def gamma(shape, scale=1.0):
    return lambda: random.gamma(shape, scale)

def geometric(p):
    return lambda: random.geometric(p)

def gumbel(loc=0.0, scale=1.0):
    return lambda: random.gumbel(loc, scale)

def hypergeometric(ngood, nbad, nsample):
    return lambda: random.hypergeometric(ngood, nbad, nsample)

def laplace(loc=0.0, scale=1.0):
    return lambda: random.laplace(loc, scale)

def logistic(loc=0.0, scale=1.0):
    return lambda: random.logistic(loc, scale)

def lognormal(mean=0.0, sigma=1.0):
    return lambda: random.lognormal(mean, sigma)

def logseries(p):
    return lambda: random.logseries(p)

def multinomial(p, pvals):
    return lambda: random.multinomial(p, pvals)

def multivariate_normal(mean, cov):
    return lambda: random.multivariate_normal(mean, cov)

def negative_binomial(n, p):
    return lambda: random.negative_binomial(n, p)

def noncentral_chisquare(df, nonc):
    return lambda: random.noncentral_chisquare(df, nonc)

def noncentral_f(dfnum, dfden, nonc):
    return lambda: random.noncentral_f(dfnum, dfden, nonc)

def normal(loc=0.0, scale=1.0):
    return lambda: random.normal(loc, scale)

def pareto(a):
    return lambda: random.pareto(a)

def poisson(lam=1.0):
    return lambda: random.poisson(lam)

def power(a):
    return lambda: random.power(a)

def rayleigh(scale = 1.0):
    return lambda: random.rayleigh(scale)

def standard_cauchy():
    return lambda: random.standard_cauchy()

def standard_exponential():
    return lambda: random.standard_exponential()

def standard_gamma(shape):
    return lambda: random.standard_gamma(shape)

def standard_normal():
    return lambda: random.standard_normal()

def standard_t(df):
    return lambda: random.standard_t(df)

def triangular(left, mode, right):
    return lambda: random.triangular(left, mode, right)

def vonmises(mu=0.0, kappa=1.0):
    return lambda: random.vonmises(mu, kappa)

def wald(mean, scale):
    return lambda: random.wald(mean, scale)

def weibull(a):
    return lambda: random.weibull(a)

def zipf(a):
    return lambda: random.zipf(a)


def composite(lst):
    distr = []
    s = 0.0
    for d in lst:
        distr.append((s, d[1]))
        s += d[0]
    distr.reverse()

    def df():
        x = random.uniform(0, 1)
        for d in distr:
            if x > d[0]:
                return d[1]()

    return df
